let points = [];
let circleRadius = 30;
let currentPosition;
let correctTurns = 0;
let isStarted = false;

function setup() {
  let wid = 400;
  let hig = 400;
  let x,y;
  
  createCanvas(wid, hig);
  
  
  x = wid/4;y = hig/8;
  fill('#99ff99');
  circle(x,y,circleRadius);
  textSize(20);
  fill('black');
  text('A', x-7, y+7);
  
  let tempX = x,tempY = y;
  points.push({x,y});
  
  x = x;y = y+2*y;
 fill('#99ff99');
  circle(x,y,circleRadius);
  textSize(20);
  fill('black');
  text('C', x-7, y+7);
  
  points.push({x,y});
  
  x = x;y = y+2*tempY;
 fill('#99ff99');
  circle(x,y,circleRadius);
  textSize(20);
   fill('black');
  text('E', x-7, y+7);
  points.push({x,y});
  
  x = wid - tempX;y = tempY;
 fill('#99ff99');
  circle(x,y,circleRadius);
   fill('black');
  textSize(20);
  text('B', x-7, y+7);
  points.push({x,y});
  
  x = x;y = y+2*y;
 fill('#99ff99');
  circle(x,y,circleRadius);
  textSize(20);
   fill('black');
  text('D', x-7, y+7);
  points.push({x,y});
  
  x = x;y = y+2*tempY;
 fill('#99ff99');
  circle(x,y,circleRadius);
  textSize(20);
 fill('black');
  text('F', x-7, y+7);
  points.push({x,y});
  
  strokeWeight(5);
  
}

function checkPosition(x, y){
  let onCorrectPosition = false;
  let xI,yI,xL,yL;
  
  for(let i = 0;i < points.length;++i){
    xI = points[i].x-(circleRadius/2);
    yI = points[i].y-(circleRadius/2);
    xL = points[i].x+(circleRadius/2);
    yL = points[i].y+(circleRadius/2);
    
    if (x > xI && x < xL && y > yI && y < yL){
      onCorrectPosition = true;
      currentPosition = i+1;
      break;
    }
  }
  
  if(onCorrectPosition == true){
    return true;
  } else {
    return false;
  }
}

function checkIniFin(ini, fin){
  if((ini == 1 && fin == 4) || (ini == 2 && fin == 5) || (ini == 3 && fin == 6)){
    
    return true;
    
  } else{
    return false;
  }
}

function checkTurn(){
  let initialPosition = currentPosition;
  console.log(currentPosition);
  
  if( checkPosition(mouseX, mouseY) == true ){
    let finalPosition = currentPosition;
   console.log(currentPosition);
    if (checkIniFin(initialPosition, finalPosition) == true){
      
      correctTurns = correctTurns + 1;
    
    }
  }
}

function mouseReleased() {
  checkTurn();
  isStarted = false;
}

function checkGraph(){
  
  console.log(correctTurns);
  
  if (correctTurns >= 3){
    alert('Right Connections');
  } else {
    alert('Wrong Connections');
  }
}

function resetGraph(){
  
  alert('all connection will be erased. are you sure');

  location.reload();
  
}

function draw() {
  
  if (mouseIsPressed) {
    
    if (isStarted == false && checkPosition(mouseX, mouseY) == false){
      
    
    } else {
      
      isStarted = true;
      stroke(237, 34, 93);
      line(mouseX , mouseY, pmouseX, pmouseY);
      
    }
    console.log(currentPosition);
    
  }
  
}