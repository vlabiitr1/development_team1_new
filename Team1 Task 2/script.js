// Driver function
function addValue(){
    // getting value of input fields :
    let xInput = document.getElementById("x").value;
    let yInput = document.getElementById("y").value;
    console.log(xInput);
    console.log(yInput);
    console.log(typeof yInput);
    if(xInput == null || yInput == null || xInput.length == 0 || yInput.length == 0)
        alert("please enter value in both the fields.");
    else if(xInput.match(/[a-z]/i) || yInput.match(/[a-z]/i))
        alert("please enter numeric value only.")
    else {
        let operation = parseInt(xInput) > parseInt(yInput);

        //Now call function to add rows
        addRows("Thisistable",xInput,yInput,operation);
    }
}

// function to add rows dynamically
function addRows(tableID,xInput,yInput,operation) {

    let tableRef = document.getElementById(tableID);
        // Insert a row at the end of the table
        let newRow = tableRef.insertRow(-1);
    
        // Insert a cell in the row at index 0,1,2
        let newCell0 = newRow.insertCell(0);
        let newCell1 = newRow.insertCell(1);
        let newCell2 = newRow.insertCell(2);
    
        // Append a text node to the cell 0
        let newText0 = document.createTextNode(String(xInput));
        newCell0.appendChild(newText0);

        // Append a text node to the cell 1
        let newText1 = document.createTextNode(String(yInput));
        newCell1.appendChild(newText1);

        // Append a text node to the cell 2
        let newText2 = document.createTextNode(String(operation));
        newCell2.appendChild(newText2);
}